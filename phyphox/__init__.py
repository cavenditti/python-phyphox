from urllib.request import Request, urlopen
import json
import urllib.parse
import time
import datetime
import logging
from urllib.error import URLError, HTTPError

def urlmerge(*args, params={}):
    """ urlmerge

    """
    url = urllib.parse.urljoin(*args)
    if params:
        url += '?'
        p_list = []
        for p in params:
            assert p and p != ''
            p_list.append(str(p) + '=' + str(params[p]) if params[p] and str(params[p]) != '' else str(p))
        url += '&'.join(p_list)
    logging.debug(url)
    return url

def req(*args, **kwargs):
    """ urllib request call wrapper
    """
    try:
        if 'params' in kwargs:
            params=kwargs['params']
            params = params if type(params) == dict else {params : ''}
            del(kwargs['params'])
            r = urllib.request.Request(urlmerge(*args, params=params), **kwargs)
        else:
            r = urllib.request.Request(urlmerge(*args), **kwargs)
        return json.loads(urllib.request.urlopen(r).read())
    except (URLError,HTTPError) as e:
        raise e
    except Exception as e:
        logging.warn(f'request exception: {e.__class__.__name__} {e}')
        return {}

def buffer_pipe(fn):
    """ Decorator for functions continuously streaming data from buffer(s) """
    def decorated(self, buffers : Buffer, *args, **kwargs):
        def sane_buffer(buffer): # if string containing buffer name return the buffer object
            return self.buffers[buffer] if buffer in self.buffers else buffer
        try:
            if type(buffers) == str:
                raise TypeError
            for i in range(len(buffers)):
                buffers[i] = sane_buffer(buffers[i])
        except:
            buffers = [sane_buffer(buffers)] # also accept a single buffer as argument
        print(f'Starting {fn.__name__} on {buffers}')
        try:
            return fn(self, buffers, *args, **kwargs)
        except KeyboardInterrupt:
            print(f'Ending {fn.__name__} on "{buffers}"')
    return decorated

class Dummy_queue:
    def put(self, *args, **Kwargs):
        pass

    def put_nowait(self, *args, **Kwargs):
        pass


class Buffer:
    def __init__(self, experiment, name):
        self.experiment = experiment
        self.name = name
        self.label = ''

    def __str__(self) -> str:
        if self.label != '':
            return f'<Buffer {self.name}: "{self.label}">'
        else:
            return f'<Buffer {self.name}>'

    def __repr__(self) -> str:
        return self.__str__()

    @property
    def last(self):
        return self.get_last()

    def get_last(self):
        """last."""
        r = self.experiment.get(params=self.name)
        return r['buffer'][self.name]['buffer'][-1]

    @property
    def threshold(self):
        """threshold."""
        r = self.experiment.get(params={self.name : self.threshold})
        return r['buffer'][self.name]['buffer']

    def monitor(self):
        """monitor."""
        self.experiment.monitor(self)


class Experiment:
    def __init__(self, url):
        self.url = url
        # retrieve experiment configuration
        self.config = req(self.url, 'config')
        for prop in self.config:
            if prop not in ['buffers', 'inputs', 'export']:
                self.__dict__[prop] = self.config[prop]

        self.buffers = {}
        for buf in self.config['buffers']:
            self.buffers[buf['name']] = Buffer(self, buf['name'])
            # also insert as a property of the experiment object
            self.__dict__[buf['name']] = self.buffers[buf['name']]

        # add labels
        for entry in self.config['export'][0]['sources']:
            if entry['buffer'] in self.buffers:
                self.buffers[entry['buffer']].label = entry['label']

    def __str__(self) -> str:
        return f'<Experiment {self.crc32}: "{self.title}">'

    def __repr__(self) -> str:
        return self.__str__()

    def __get_call(self, name, *args, **kwargs):
        return req(self.url, name, *args, **kwargs)

    def _cmd(self, cmd):
        #print(f'cmd: {cmd}')
        return self.__get_call('control', params={'cmd' : cmd})

    def start(self):
        """start."""
        self._cmd('start')

    def stop(self):
        """stop."""
        self._cmd('stop')

    def clear(self):
        """clear."""
        self._cmd('clear')

    def get(self, *args, **kwargs):
        """get.

        :param args:
        :param kwargs:
        """
        r = self.__get_call('get', *args, **kwargs)
        self._last_status = r['status']
        return r

    def get_config(self, *args, **kwargs):
        return self.__get_call('config', *args, **kwargs)

    @property
    def _t_buffer(self):
        return self.config['inputs'][0]['outputs'][-1]['t']

    @property
    def time(self):
        return self.__dict__[self._t_buffer]

    def print_config(self):
        print(json.dumps(self.config, indent=2))

    @property
    def status(self):
        """status."""
        return self.get()['status']

    @property
    def measuring(self):
        """measuring."""
        return self.status['measuring']

    def _last_measuring(self):
        return self._last_status['measuring']

    def _fetch(self, buffers : Buffer, sink):
        """ Fetches data and flushes everything down the sink.
            A sink can be anything with an append method, such as lists or deques
        """

        last_time = 0
        while True:
            params_str = f'{self._t_buffer}={last_time}'
            for buffer in buffers:
                params_str += f'&{buffer.name}={last_time}|{self._t_buffer}'
            r = self.get(params=params_str)
            r_t = r['buffer'][self._t_buffer]['buffer']
            r_b = {buffer:r['buffer'][buffer.name]['buffer'] for buffer in buffers}
            if r_t:
                last_time = r_t[-1]
                for i in range(len(r_b[buffers[0]])):
                    sink.append([r_t[i]] + [r_b[buffer][i] for buffer in buffers])

    @buffer_pipe
    def fetch(self, buffers : Buffer, sink):
        return self.fetch(buffers, sink)

    @buffer_pipe
    def save_dataframe(self, buffers : Buffer):
        """ Save DataFrame from selected buffers """
        from pandas import DataFrame

        tmp_list = []

        try:
            self._fetch(buffers, tmp_list)
        except KeyboardInterrupt:
            print(f'Saving DataFrame from "{buffers}"')
            return DataFrame(data=tmp_list,columns=['time']+[buffer.name for buffer in buffers])

    @buffer_pipe
    def monitor(self, buffer : Buffer, resolution : float = 0.):
        """monitors a single buffer, useful for quick tests

        :param buffer: buffer to monitor
        :type buffer: Buffer
        :param resolution: update frequency in seconds
        :type resolution: float
        """

        while True:
            last = buffer.last
            if last is None:
                continue
            print(last)
            time.sleep(resolution)
            while not self._last_measuring():
                time.sleep(resolution + 0.5)
                self.get()

